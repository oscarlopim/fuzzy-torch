from __future__ import print_function
from __future__ import division
from . import C_

class NanLossError(Exception):
	def __init__(self):
		pass
		
class TrainingStopError(Exception):
	def __init__(self):
		pass